unit NUMHelper;

interface

uses
  System.SysUtils, System.Variants;

function ConvVarToInt(const AValue: Variant): Integer;
function ConvVarToFloat(const AInput: Variant): Double;

implementation

function ConvVarToInt(const AValue: Variant): Integer;
begin
  Result := 0;
  if not VarIsNull(AValue) then
  begin
    // ���� ��������...
    if VarIsOrdinal(AValue) then
      Result := StrToInt(VarToStr(AValue));
    if VarIsStr(AValue) then
      Result := StrToIntDef(AValue, 0);
  end;
end;

function ConvVarToFloat(const AInput: Variant): Double;
var
  NStr: string;
begin
  Result := 0;
  NStr := VarToStr(AInput);
  NStr := StringReplace(NStr, ' ', '', [rfReplaceAll]);
  NStr := StringReplace(NStr, '.', FormatSettings.DecimalSeparator, [rfReplaceAll]);
  NStr := StringReplace(NStr, ',', FormatSettings.DecimalSeparator, [rfReplaceAll]);
  Result := StrToFloatDef(NStr, 0);
end;

end.
