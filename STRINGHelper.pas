unit STRINGHelper;

interface

uses
  System.SysUtils, System.Classes, System.RegularExpressions, IdHashMessageDigest;

function GeneratePassword(ALength: Integer): string;
function DelDoubleSpaces(const AString: string): string;
function DelAllSpaces(const AString: string): string;
function ParseTagsString(const ATags: string): TStringList;
function IsGUID(const AString: string): Boolean;
function UpperCaseFirst(const AString: string): string;
function GetMD5Hash(AString: String): String;
function DelInvalidCharsFromString(AString: String): String;
function AddTrailingPathDelimiter(AString: string): String;
function StringForInCondition(AStrList: TStringList): String;
function IsCadastreNum(const AString: String): Boolean;
function IsSnils(const AString: string): Boolean;
function IsInn(const AString: string): Boolean;
procedure AddToStringList(var AStringList: TStringList; AString: string); overload;
procedure AddToStringList(var AStringList: TStringList; AStrings: TStrings); overload;

implementation

function GeneratePassword(ALength: Integer): string;
const
  s = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var
  I: Integer;
begin
  for I := 1 to ALength do
    Result := Result + s[Random(Length(s)) + 1];
end;

function DelDoubleSpaces(const AString: string): string;
begin
  Result := Trim(AString);
  while Pos('  ', Result) > 0 do
    Result := StringReplace(Result, '  ', ' ', [rfReplaceAll]);
end;

function DelAllSpaces(const AString: string): string;
begin
  Result := AString;
  while pos(' ', Result) > 0 do
    delete(Result, pos(' ', Result), 1);
end;

function ParseTagsString(const ATags: string): TStringList;
const
  tagMask = '#[a-z0-9�-�]+';
var
  NMatch: TMatch;
begin
  Result := TStringList.Create;
  NMatch := TRegEx.Match(ATags, tagMask, [roIgnoreCase]);
  while NMatch.Success do
  begin
    if Result.IndexOf(NMatch.Value) < 0 then
      Result.Add(NMatch.Value);
    NMatch := NMatch.NextMatch;
  end;
end;

function IsGUID(const AString: string): Boolean;
const
  Mask = '^\{?[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\}?$';
  // ����� ���������� ����� ����������� � �������� ������
begin
  Result := TRegEx.IsMatch(Trim(AString), Mask, [roIgnoreCase]);
end;

function UpperCaseFirst(const AString: string): string;
begin
  Result := EmptyStr;
  if Length(Trim(AString)) > 0 then
  begin
    Result := AnsiLowerCase(Trim(AString));
    Result[1]:=AnsiUpperCase(Result[1])[1];
  end;
end;

function GetMD5Hash(AString: String): String;
var
  HashMsgDigest: TIdHashMessageDigest;
begin
  HashMsgDigest := TIdHashMessageDigest5.Create;
  try
    Result := HashMsgDigest.HashStringAsHex(AString);
  finally
    HashMsgDigest.Free;
  end;
end;

function DelInvalidCharsFromString(AString: String): String;
const
  InvalidChars: array [0..8] of Char = ('\', '/', ':', '*', '?', '"', '<', '>', '|');
var
  I: integer;
begin
  Result := AString;
  for I:=0 to High(InvalidChars)-1 do
    Result := StringReplace(Result, InvalidChars[I], '', [rfReplaceAll]);
end;

function AddTrailingPathDelimiter(AString: string): String;
begin
  Result := Trim(AString);
  if Result <> EmptyStr then
    Result := IncludeTrailingPathDelimiter(AString); // ��! AString! � �� ������
end;

function StringForInCondition(AStrList: TStringList): String;
var
  I: Integer;
begin
  Result := EmptyStr;
  for I := 0 to AStrList.Count - 1 do
  begin
    if Result <> EmptyStr then
      Result := Result + ',';
    Result := Result + QuotedStr(AStrList[I]);
  end;

  // ��� ������, �������� ������ ��� ������
  // Result := StringReplace(AStrList.DelimitedText, ',', ''',''', [rfReplaceAll]);
  // Result := '''' + Result + '''';
end;

function IsCadastreNum(const AString: String): boolean;
const
  CadastreMask = '^\d+:\d+:\d+:\d+$';
begin
  Result := TRegEx.IsMatch(AString, CadastreMask);
end;

function IsInn(const AString: string): Boolean;
const
  InnMask = '^\d{10,}$';  // 10 ���� � �����
begin
  Result := TRegEx.IsMatch(AString, InnMask);
end;

function IsSnils(const AString: string): Boolean;
const
  SnilsMask = '^\d{3}-\d{3}-\d{3}[\s-]\d{2}$';
begin
  Result := TRegEx.IsMatch(AString, SnilsMask);
end;

procedure AddToStringList(var AStringList: TStringList; AString: string);
begin
  if AStringList.IndexOf(AString) < 0 then
    AStringList.Add(AString);
end;

procedure AddToStringList(var AStringList: TStringList; AStrings: TStrings);
var
  I: Integer;
begin
  for I := 0 to AStrings.Count - 1 do
  begin
    if AStringList.IndexOf(AStrings.Strings[I]) < 0 then
      AStringList.Add(AStrings.Strings[I]);
  end;
end;

end.
