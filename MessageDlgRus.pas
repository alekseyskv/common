unit MessageDlgRus;

interface

implementation

uses
  Classes, Forms, Consts, Dialogs; //, RtpCtrls;

resourcestring
  SMsgDlgRusWarning = '��������������';
  SMsgDlgRusError = '������';
  SMsgDlgRusInformation = '����������';
  SMsgDlgRusConfirm = '�������������';
  SMsgDlgRusYes = '&��';
  SMsgDlgRusNo = '�&��';
  SMsgDlgRusOK = 'OK';
  SMsgDlgRusCancel = '������';
  SMsgDlgRusHelp = '&������';
  SMsgDlgRusHelpNone = '������ ����������';
  SMsgDlgRusHelpHelp = '������';
  SMsgDlgRusAbort = '&��������';
  SMsgDlgRusRetry = '��&�������';
  SMsgDlgRusIgnore = '&������������';
  SMsgDlgRusAll = '&���';
  SMsgDlgRusNoToAll = 'H&�� ��� ����';
  SMsgDlgRusYesToAll = '�&� ��� ����';

const
  Captions: array[TMsgDlgType] of Pointer = (
    @SMsgDlgRusWarning,
    @SMsgDlgRusError,
    @SMsgDlgRusInformation,
    @SMsgDlgRusConfirm,
    nil // mtCustom
  );
  ButtonCaptions: array[TMsgDlgBtn] of Pointer = (
    @SMsgDlgRusYes,
    @SMsgDlgRusNo,
    @SMsgDlgRusOK,
    @SMsgDlgRusCancel,
    @SMsgDlgRusAbort,
    @SMsgDlgRusRetry,
    @SMsgDlgRusIgnore,
    @SMsgDlgRusAll,
    @SMsgDlgRusNoToAll,
    @SMsgDlgRusYesToAll,
    @SMsgDlgRusHelp,
    nil // mbClose
  );

procedure _ChangeCaptions(List: PPointerList; Last: Pointer);
var
  I, Max: Integer;
  IsFind: Boolean;
begin
  Max := (Integer(Last)-Integer(List)) div SizeOf(Pointer);
  IsFind := False;
  for I := 0 to Max - 2 do
    if (List[I] = @SMsgDlgWarning) and (List[I+2] = @SMsgDlgInformation) then
    begin
      IsFind := True;
      Break;
    end;

    if IsFind then
      Move(Captions, List[I], SizeOf(Captions));

    IsFind := False;
    for I := I to Max - 2 do
      if (List[I] = @SMsgDlgYes) and (List[I+2] = @SMsgDlgOK) then
      begin
        IsFind := True;
        Break;
      end;

    if IsFind then
      Move(ButtonCaptions, List[I], SizeOf(ButtonCaptions));
end;

initialization

_ChangeCaptions(@DebugHook, @Application);

end.


