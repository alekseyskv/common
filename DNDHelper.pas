unit DNDHelper;

// https://habr.com/post/179131/

interface

uses System.Types, Vcl.Controls, Winapi.ShellAPI;

type
 TFileCatcher = class(TObject)
  private
    FDropHandle: HDROP;
    function GetFile(Idx: Integer): string;
    function GetFileCount: Integer;
    function GetPoint: TPoint;
    function GetDropControl: TControl;
  public
    constructor Create(ADropHandle: HDROP);
    destructor Destroy; override;
    property FileCount: Integer read GetFileCount;
    property Files[Idx: Integer]: string read GetFile;
    property DropPoint: TPoint read GetPoint;
    property DropControl: TControl read GetDropControl;
  end;


implementation

{ TFileCatcher }

constructor TFileCatcher.Create(ADropHandle: HDROP);
begin
  inherited Create;
  FDropHandle := ADropHandle;
end;

destructor TFileCatcher.Destroy;
begin
  DragFinish(FDropHandle);
  inherited;
end;

function TFileCatcher.GetDropControl: TControl;
var
  Container: TWinControl; // reference to control represented by component
  Pt: TPoint;             // drop point in screen co-ordinates
  ChildCtrl: TControl;    // reference to child control containing point
begin
  // Assume there's no control under drop point
  Result := nil;
{
  // Begin with control window associated with drop files control
  Container := GetContainer;
  if Assigned(Container) then
  begin
    // Check that drop point is in container: no control if not
    if PtInRect(Container.ClientRect, fDropPoint) then
    begin
      // Record drop point in screen co-ordinates
      Pt := Container.ClientToScreen(fDropPoint);
      // Find inner-most child control that is child of container
      ChildCtrl := Container;
      while (ChildCtrl <> nil) do
      begin
        Result := ChildCtrl;
        if Result is TWinControl then
          // we've found a windowed control containing point:
          // check if any of it's child controls also contain it
          ChildCtrl := FindCtrl(Result as TWinControl, Pt)
        else
          ChildCtrl := nil;
      end;
    end;
  end;
}
end;

function TFileCatcher.GetFile(Idx: Integer): string;
var
  NFileNameLength: Integer;
begin
  NFileNameLength := DragQueryFile(FDropHandle, Idx, nil, 0);
  SetLength(Result, NFileNameLength);
  DragQueryFile(FDropHandle, Idx, PChar(Result), NFileNameLength + 1);
end;

function TFileCatcher.GetFileCount: Integer;
begin
  Result := DragQueryFile(FDropHandle, $FFFFFFFF, nil, 0);
end;

function TFileCatcher.GetPoint: TPoint;
begin
  DragQueryPoint(FDropHandle, Result);
end;

end.
