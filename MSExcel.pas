﻿unit MSExcel;

interface

uses
  System.SysUtils, System.Variants, System.Win.ComObj, Winapi.ActiveX;

type
  TExcel = class(TObject)
    FExcel: OleVariant;     // Application Object (Excel)
    FWorkBook: OleVariant;  // Workbook Object (Excel)
    FWorkSheet: OleVariant; // Worksheet Object (Excel)
    public
      property WorkBook: OleVariant read FWorkBook write FWorkBook;
      property WorkSheet: OleVariant read FWorkSheet write FWorkSheet;

      constructor Create(AVisible: Boolean = False);
      destructor Destroy; override;
      function Quit: Boolean;
      function NewBook: Boolean; overload;
      function NewBook(ATempalePath: string): Boolean; overload;
      function OpenBook(AFilePath: string; AReadOnly: Boolean = true): Boolean;
      function GetCellValue(ARow, ACol: Integer): Variant;
      procedure SetCellValue(ARow, ACol: Integer; AValue: Variant);
      procedure HorizontalMerge(ARow, AColStart, AColCount: Integer);
      procedure VerticalMerge(ACol, ARowStart, ARowCount: Integer);
      procedure Show;
      procedure Hide;
      procedure CloseAllBooks;
      class function CheckInstall: Boolean; static;
  end;

const
  ExcelApp = 'Excel.Application';

implementation

{ TExcel }

class function TExcel.CheckInstall: Boolean;
var
  ClassID: TCLSID;
  Res : HRESULT;
begin
  Res := CLSIDFromProgID(PWideChar(WideString(ExcelApp)), ClassID);
  Result := (Res = S_OK);
end;

function TExcel.Quit: Boolean;
begin
  try
    FExcel.Visible := False;
    FExcel.Quit;
    Result := True;
  except
    Result := False;
  end;
end;

procedure TExcel.SetCellValue(ARow, ACol: Integer; AValue: Variant);
begin
  try
    FWorkSheet.Cells[ARow, ACol] := AValue;
  except
    raise Exception.Create('Ошибка заполнения ячейки');
  end;
end;

procedure TExcel.Show;
begin
  FExcel.Visible := True;
end;

procedure TExcel.VerticalMerge(ACol, ARowStart, ARowCount: Integer);
begin
  if (ACol < 1) or (ARowStart < 1) or (ARowCount < 0) then
    Exit;

  try
    FWorkSheet.Cells[ARowStart, ACol].Resize[ARowCount, ACol].Merge;
  except
    raise Exception.Create('Ошибка объединения ячеек');
  end;
end;

procedure TExcel.Hide;
begin
  FExcel.Visible := True;
end;

procedure TExcel.HorizontalMerge(ARow, AColStart, AColCount: Integer);
begin
  if (ARow < 1) or (AColStart < 1) or (AColCount < 0) then
    Exit;

  try
    FWorkSheet.Cells[ARow, AColStart].Resize[ARow, AColCount].Merge;
  except
    raise Exception.Create('Ошибка объединения ячеек');
  end;
end;

procedure TExcel.CloseAllBooks;
var
  I: Integer;
begin
  if FExcel.WorkBooks.Count > 0 then
    FExcel.WorkBooks.Close;
//  for I:=1 to FExcel.WorkBooks.Count do
//      FExcel.WorkBooks.Item(I).Close(SaveChanges:=False);
end;

constructor TExcel.Create(AVisible: Boolean);
begin
  Inherited Create;
  FExcel := CreateOleObject(ExcelApp);
  FExcel.Visible := AVisible;
  FExcel.DisplayAlerts := False;  // Отключаем сообщения Excel
{
  // надо разобрать параметры...
  exl.Application.ScreenUpdating := False; // Обновление экрана
  exl.Application.Calculation := -4135; // xlCalculationManual - Параметры автоматического вычисления значений ячеек
  exl.Application.EnableEvents := False;  // Отключаем диалоговые окна с пользователем
  exl.Application.Interactive := False;  // Блокируем Excel
  exl.Application.DisplayAlerts := False;  // Отключаем сообщения Excel
  exl.Application.DisplayStatusBar := False;  // Убираем отображение статусной строки    Index := 1;
  exl.CalculateBeforeSave := False;
}
end;

destructor TExcel.Destroy;
begin
  FExcel := Unassigned;
  inherited;
end;

function TExcel.GetCellValue(ARow, ACol: Integer): Variant;
begin

end;

function TExcel.NewBook: Boolean;
begin
  try
    FWorkBook := FExcel.WorkBooks.Add;
    FWorkBook.Activate;
    if FWorkBook.Sheets.Count = 0 then
    begin
      FWorkSheet := FWorkBook.Sheets.Add;
      FWorkSheet.Activate;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TExcel.NewBook(ATempalePath: string): Boolean;
begin
  try
    FWorkBook := FExcel.WorkBooks.Add(Template:=ATempalePath);
    FWorkBook.Activate;
    if FWorkBook.Sheets.Count > 0 then
    begin
      FWorkSheet := FWorkBook.Sheets.Item[1];
      FWorkSheet.Activate;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TExcel.OpenBook(AFilePath: string; AReadOnly: Boolean): Boolean;
begin
  try
    FWorkBook := FExcel.WorkBooks.Open(FileName:=AFilePath, ReadOnly:=AReadOnly);
    FWorkBook.Activate;
    if FWorkBook.Sheets.Count > 0 then
    begin
      FWorkSheet := FWorkBook.Sheets.Item[1];
      FWorkSheet.Activate;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

end.
