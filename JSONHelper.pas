unit JSONHelper;

interface

uses
  System.SysUtils, System.JSON, System.Classes, System.Variants, Vcl.Dialogs, Winapi.Windows;

function IsJSONObject(const aJSON: WideString): Boolean;
function IsJSONArray(const aJSON: WideString): Boolean;

function ParseJSON(var aJO: TJSONObject; aJSON: WideString): Boolean; overload;
function ParseJSON(var aJA: TJSONArray; aJSON: WideString): Boolean; overload;
function ParseJSON(aJSON: WideString): Boolean; overload;

function ParseJSONBytes(var aJO: TJSONObject; aJSON: TBytes): Boolean; overload;
function ParseJSONBytes(var aJA: TJSONArray; aJSON: TBytes): Boolean; overload;
function ParseJSONBytes(aJSON: TBytes): Boolean; overload;

function type2jv(aValue: TVarRec): TJSONValue;
function buildJA(aValue: array of const): TJSONArray;
function buildJO(aValue: array of const): TJSONObject;
function buildJOS(aValue: array of const): WideString;
function buildJAS(aValue: array of const): WideString;


function JSONArray(aJSON: WideString = ''): TJSONArray;
function JSONObject(aJSON: WideString = ''): TJSONObject;

function JPE(aJO: TJSONObject; aPair: WideString): Boolean; overload;
function JPE(aJO: WideString; aPair: WideString): Boolean; overload;
function JVE(aJA: TJSONArray; aValue: Variant): Boolean;

function AddPair(aJO: TJSONObject; aPair: WideString; aValue: Variant): Boolean;

function GetPair(aJO: TJSONObject; aPair: WideString): TJSONValue;

function RemovePair(var aJO: TJSONObject; aPair: WideString): Boolean;

// jsonvalue to variant
function j2v(aValue: TJSONValue): Variant;
// variant to jsonvalue
function v2j(aValue: Variant): TJSONValue;
// get pair value to variant
function p2v(aJSON: TJSONValue; aPair: WideString): Variant; overload;
function p2v(aJSON, aPair: WideString): Variant; overload;
// get item value to variant
function i2v(aJSON: TJSONArray; aItem: Integer): Variant; overload;
function i2v(aJSON: WideString; aItem: Integer): Variant; overload;

function EscapeChars(aString: string): string;
function FindPair(aJO: TJSONObject; aPair: string): string;

implementation

function IsJSONObject(const aJSON: WideString): Boolean;
var
  WS: WideString;
begin
  WS := Trim(aJSON);
  if not(Length(WS) > 2) then
    Exit(False);
  Result := ((WS[1] = '{') and (WS[High(WS)] = '}') and ParseJSON(aJSON));
end;

function IsJSONArray(const aJSON: WideString): Boolean;
var
  WS: WideString;
begin
  WS := Trim(aJSON);
  if not Length(WS) > 2 then
    Exit(False);
  Result := ((WS[1] = '[') and (WS[High(WS)] = ']') and ParseJSON(aJSON));
end;

function ParseJSON(var aJO: TJSONObject; aJSON: WideString): Boolean;
begin
  try
    aJO := TJSONObject(TJSONObject.ParseJSONValue(aJSON));
    Result := Assigned(aJO);
  except
    Result := False;
  end;
end;

function ParseJSON(var aJA: TJSONArray; aJSON: WideString): Boolean;
begin
  try
    aJA := TJSONArray(TJSONObject.ParseJSONValue(aJSON));
    Result := Assigned(aJA);
  except
    Result := False;
  end;
end;

function ParseJSONBytes(var aJO: TJSONObject; aJSON: TBytes): Boolean;
begin
  try
    aJO := TJSONObject(TJSONObject.ParseJSONValue(aJSON, 0, False));
    Result := Assigned(aJO);
  except
    Result := False;
  end;
end;

function ParseJSONBytes(var aJA: TJSONArray; aJSON: TBytes): Boolean;
begin
  try
    aJA := TJSONArray(TJSONObject.ParseJSONValue(aJSON, 0));
    Result := Assigned(aJA);
  except
    Result := False;
  end;
end;

function ParseJSONBytes(aJSON: TBytes): Boolean;
var
  JO: TJSONObject;
begin
  try
    JO := TJSONObject(TJSONObject.ParseJSONValue(aJSON, 0));
    Result := Assigned(JO);
    JO.Free;
  except
    Result := False;
  end;
end;

function ParseJSON(aJSON: WideString): Boolean;
var
  JO: TJSONObject;
begin
  try
    JO := TJSONObject(TJSONObject.ParseJSONValue(aJSON));
    Result := Assigned(JO);
    JO.Free;
  except
    Result := False;
  end;
end;

function buildJA(aValue: array of const): TJSONArray;
var
  P: Integer;
begin
  Result := TJSONArray.Create;
  if Length(aValue) = 0 then
    Exit(Result);
  try
    for P := 0 to Length(aValue) - 1 do
      Result.AddElement(type2jv(aValue[P]));
  except
    Result.Free;
    Result := nil;
  end;
end;

function buildJO(aValue: array of const): TJSONObject;
var
  P, Pair, Value: Integer;
begin
  if (Length(aValue) = 0) and (Length(aValue) < 2) then
    Exit;
  try
    Result := TJSONObject.Create;
    for P := 0 to (Length(aValue) div 2) - 1 do
    begin
      Pair := P * 2;
      Value := P * 2 + 1;
      Result.AddPair(aValue[Pair].VPWideChar, type2jv(aValue[Value]));
    end;
  except
    Result.Free;
    Result := nil;
  end;
end;

function buildJOS(aValue: array of const): WideString;
var
  JO: TJSONObject;
begin
  Result := '{}';
  if (Length(aValue) = 0) and (Length(aValue) < 2) then
    Exit;
  try
    JO := buildJO(aValue);
    if Assigned(JO) then
      Result := JO.ToString; //ToJSON;
  finally
    JO.Free;
  end;
end;

function buildJAS(aValue: array of const): WideString;
var
  JA: TJSONArray;
begin
  Result := '[]';
  if Length(aValue) = 0 then
    Exit;
  try
    JA := buildJA(aValue);
    if Assigned(JA) then
      Result := JA.ToString; //ToJSON;
  finally
    JA.Free;
  end;
end;

function type2jv(aValue: TVarRec): TJSONValue;
begin
  case aValue.VType of
    vtInteger:
      Result := TJsonNumber.Create(aValue.VInteger);
    vtInt64:
      Result := TJsonNumber.Create(aValue.VInt64^);
    vtExtended:
      Result := TJsonNumber.Create(Extended(aValue.VExtended^));
    vtCurrency:
      Result := TJsonNumber.Create(Currency(aValue.VCurrency^));
    vtBoolean:
      case aValue.VBoolean of
        True:
          Result := TJSONTrue.Create;
        False:
          Result := TJSONFalse.Create;
      end;
    vtString:
      Result := TJSONString.Create(String(aValue.VWideString));
    vtAnsiString:
      Result := TJSONString.Create(WideString(aValue.VWideString));
    vtWideString:
      Result := TJSONString.Create(WideString(aValue.VWideString));
    vtUnicodeString:
      Result := TJSONString.Create(UnicodeString(aValue.VUnicodeString));
    vtChar:
      Result := TJSONString.Create(WideString(aValue.VWideString));
    vtObject:
      if aValue.VObject.ClassType = TJSONObject then
        Result := TJSONObject(aValue.VObject)
      else if aValue.VObject.ClassType = TJSONArray then
        Result := TJSONArray(aValue.VObject);
    vtVariant:
      Result := v2j(aValue.VVariant^);
  end;
end;

function JSONArray(aJSON: WideString = ''): TJSONArray;
begin
  if not ParseJSON(Result, aJSON) then
    Result := TJSONArray.Create;
end;

function JSONObject(aJSON: WideString = ''): TJSONObject;
begin
  if not ParseJSON(Result, aJSON) then
    Result := TJSONObject.Create;
end;

function JPE(aJO: TJSONObject; aPair: WideString): Boolean;
var
  JV: TJSONValue;
begin
  JV := nil;
  Result := False;
  if Assigned(aJO) then
  begin
    JV := aJO.GetValue(aPair);
    Result := Assigned(JV);
  end;
end;

function JPE(aJO: WideString; aPair: WideString): Boolean;
var
  JO: TJSONObject;
begin
  Result := False;
  if ParseJSON(JO, aJO) then
    try
      Result := JPE(JO, aPair);
    finally
      JO.Free;
    end;
end;

function JVE(aJA: TJSONArray; aValue: Variant): Boolean;
var
  J: Integer;
begin
  Result := False;
  if Assigned(aJA) then
    for J := 0 to aJA.Count - 1 do
      if i2v(aJA, J) = aValue then
      begin
        Result := True;
        Break;
      end;
end;

function AddPair(aJO: TJSONObject; aPair: WideString; aValue: Variant): Boolean;
begin
  Result := False;
  if Assigned(aJO) then
    Result := Assigned(aJO.AddPair(aPair, v2j(aValue)));
end;

function GetPair(aJO: TJSONObject; aPair: WideString): TJSONValue;
begin
  Result := nil;
  if Assigned(aJO) then
    Result := aJO.GetValue(aPair);
end;

function RemovePair(var aJO: TJSONObject; aPair: WideString): Boolean;
begin
  Result := False;
  if Assigned(aJO) then
  begin
    aJO.RemovePair(aPair);
    Result := True;
  end;
end;

function j2v(aValue: TJSONValue): Variant;
begin
  Result := Unassigned;
  if aValue is TJSONObject then
    if (TJSONObject(aValue).ToSTRING = '{}') then
      Result := '{}'
    else
      Result := TJSONObject(aValue).ToSTRING
  else if aValue is TJSONArray then
    if (TJSONArray(aValue).ToSTRING = '[]') then
      Result := '[]'
    else
      Result := (aValue AS TJSONArray).ToSTRING
  else if aValue is TJsonNumber then
    if Pos('.', TJsonNumber(aValue).ToSTRING) = 0 then
      Result := TJsonNumber(aValue).AsInt64
    else
      Result := TJsonNumber(aValue).AsDouble
  else if aValue is TJSONString then
    Result := TJSONString(aValue).Value
  else if aValue is TJSONTrue then
    Result := True
  else if aValue is TJSONFalse then
    Result := False;
end;

function v2j(aValue: Variant): TJSONValue;
var
  JA: TJSONArray;
  JO: TJSONObject;
  VType: Integer;
begin
  Result := nil;
  VType := varType(aValue) and varTypeMask;
  case VType of
    varInteger, varInt64, varUInt64, varDouble, varWord, varSingle, varShortInt, varSmallInt, varCurrency:
      Result := TJsonNumber.Create(aValue);
    varNull:
      Result := TJSONNull.Create;
    varBoolean:
      case Boolean(aValue) of
        True:
          Result := TJSONTrue.Create;
        False:
          Result := TJSONFalse.Create;
      end;
    varString, varUString, varByte:
      if not ParseJSON(TJSONObject(Result), aValue) then
        Result := TJSONString.Create(aValue);
    varOleStr:
      if ParseJSON(aValue) then
        Result := TJSONObject.ParseJSONValue(aValue)
      else
        Result := TJSONString.Create(aValue);
  else
    Result := TJSONNull.Create;
  end;
end;

function p2v(aJSON: TJSONValue; aPair: WideString): Variant;
begin
  Result := Unassigned;
  if aJSON is TJSONObject then
    if JPE(TJSONObject(aJSON), aPair) then
      Result := j2v(TJSONObject(aJSON).GetValue(aPair));
end;

function p2v(aJSON, aPair: WideString): Variant;
var
  JO: TJSONObject;
begin
  Result := Unassigned;
  if ParseJSON(JO, aJSON) then
    try
      if JPE(JO, aPair) then
        Result := j2v(JO.GetValue(aPair));
    finally
      JO.Free;
    end;
end;

function i2v(aJSON: TJSONArray; aItem: Integer): Variant;
begin
  Result := Unassigned;
  if aItem <= aJSON.Count - 1 then
    Result := j2v(aJSON.Items[aItem]);
end;

function i2v(aJSON: WideString; aItem: Integer): Variant;
var
  JA: TJSONArray;
begin
  Result := Unassigned;
  if ParseJSON(JA, aJSON) then
    try
      if aItem <= JA.Count - 1 then
        Result := j2v(JA.Items[aItem]);
    finally
      JA.Free;
    end;
end;

function EscapeChars(AString: string): String;
begin
  Result := StringReplace(AString, '\', '\\', [rfReplaceAll]);
  Result := StringReplace(Result, '"', '\"', [rfReplaceAll]);
end;

function FindPair(aJO: TJSONObject; aPair: string): string;
var
  NPair: TJSONPair;
  NEnum: TJSONPairEnumerator;
begin
  Result := EmptyStr;
  NEnum := aJO.GetEnumerator;
  while NEnum.MoveNext do
  begin
    NPair := NEnum.Current;
    if String.Compare(NPair.JsonString.Value, aPair, [coIgnoreCase]) = 0
    then Result := NPair.JsonString.Value;
  end;
end;

end.
