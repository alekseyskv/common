unit DATEHelper;

interface

uses
  System.SysUtils, System.Variants, System.DateUtils, System.RegularExpressions;

function GetSystemUTC: string;
function ConvDateTimeFromSQL(ADateTimeStr: string): TDateTime;
function ConvDateTimeForSQL(ADateTime: TDateTime): string;
function ConvDateForSQL(ADateTime: TDateTime): string;
function ConvStrToDateDef(const AInput: String; const ADefault: TDate = 0): TDate;
function ConvVarToDateDef(const AInput: Variant; const ADefault: TDate = 0): TDate;
function GetRusMonth(const ADate: TDate; const AShowYear: boolean = True): string;
function GetRusDate(const ADate: TDate): string;
function GetMonthNum(AMonthName: string): Integer;
function ParseShortDateDef(const AInput: String; const ADefault: TDate = 0): TDate;
function ParseDateByMask(const AInput: String; const AMask: String): TDate;
function ParseMonthYearDateDef(const AInput: String; const ADefault: TDate = 0): TDate;

implementation

function GetSystemUTC: string;
var
  NDateTime: TDateTime;
  NLocal, NUniversal: Int64;
  NDiff: TTime;
  NSign: Char;
begin
  NSign := '-';
  NDiff := 0;
  NDateTime := Now;
  NLocal := DateTimeToUnix(NDateTime);
  NUniversal := DateTimeToUnix(NDateTime, False);
  if NLocal = NUniversal then
  begin
    Result := 'UTC';
    Exit;
  end;
  if NLocal > NUniversal then
    NSign := '+';

  NDiff := IncSecond(NDiff, Abs(NLocal - NUniversal));
  Result := 'UTC' + NSign + FormatDateTime('hh:ss', NDiff);
end;

function ConvDateTimeFromSQL(ADateTimeStr: string): TDateTime;
var
  fs: TFormatSettings;
begin
  fs := TFormatSettings.Create;

  fs.DateSeparator := '-';
  fs.TimeSeparator := ':';
  fs.ShortDateFormat := 'yyyy-mm-dd';
  fs.LongDateFormat := 'yyyy-mm-dd';
  fs.ShortTimeFormat := 'hh:mm:ss';
  fs.LongTimeFormat := 'hh:mm:ss';

  Result := StrToDateTimeDef(ADateTimeStr, 0, fs);
end;

function ConvDateTimeForSQL(ADateTime: TDateTime): string;
begin
  DateTimeToString(Result, 'yyyy-mm-dd hh:mm:ss', ADateTime);
end;

function ConvDateForSQL(ADateTime: TDateTime): string;
begin
  DateTimeToString(Result, 'yyyy-mm-dd', ADateTime);
end;

function ConvVarToDateDef(const AInput: Variant; const ADefault: TDate): TDate;
begin
  Result := ConvStrToDateDef(VarToStr(AInput), ADefault);
end;

function ConvStrToDateDef(const AInput: String; const ADefault: TDate): TDate;
const
  YMDMask = '^\d{4}[-\.](0[1-9]|1[012])[-\.](0[1-9]|[12][0-9]|3[01])$';
  DMYMask = '^(0[1-9]|[12][0-9]|3[01])[-\.](0[1-9]|1[012])[-\.]\d{4}$';
  DelimeterMask = '[-\.]';
var
  NArr: TArray<String>;
begin
  Result := ADefault;
  try
    if TRegEx.IsMatch(AInput, YMDMask) then
    begin
      NArr := TRegEx.Split(AInput, DelimeterMask);
      Result := EncodeDate(StrToInt(NArr[0]),StrToInt(NArr[1]),StrToInt(NArr[2]));
    end
    else if TRegEx.IsMatch(AInput, DMYMask) then
    begin
      NArr := TRegEx.Split(AInput,DelimeterMask);
      Result := EncodeDate(StrToInt(NArr[2]),StrToInt(NArr[1]),StrToInt(NArr[0]));
    end;
  except
    Result := ADefault;
  end;
end;

function ParseShortDateDef(const AInput: String; const ADefault: TDate = 0): TDate;
const
  ddmmyyMask = '^(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.\d{2}$';
  delimeterMask = '\.';
var
  NArr: TArray<String>;
begin
  Result := ADefault;
  try
    if TRegEx.IsMatch(AInput, ddmmyyMask) then
    begin
      NArr := TRegEx.Split(AInput, DelimeterMask);
      if Length(NArr) = 3 then
        Result := EncodeDate(2000+StrToInt(NArr[2]), StrToInt(NArr[1]), StrToInt(NArr[0]));
    end
  except
    Result := ADefault;
  end;
end;

function ParseDateByMask(const AInput: String; const AMask: String): TDate;
const
  delimeterMask = '\.';
var
  NArr: TArray<String>;
  NYear: Integer;
  NMatch: TMatch;
begin
  Result := 0;
  try
    if TRegEx.IsMatch(AInput, AMask) then
    begin
      NMatch := TRegEx.Match(AInput, AMask, [roIgnoreCase]);
      while NMatch.Success do
      begin
        NArr := TRegEx.Split(NMatch.Value, DelimeterMask, [roIgnoreCase]);
        if Length(NArr) = 3 then
        begin
          NYear := StrToInt(NArr[2]);
          if NYear < 1000
          then NYear := 2000 + NYear;
          Result := EncodeDate(NYear, StrToInt(NArr[1]), StrToInt(NArr[0]));
        end;
        NMatch := NMatch.NextMatch;
      end;
    end
  except
  end;
end;

function ParseMonthYearDateDef(const AInput: String; const ADefault: TDate = 0): TDate;
const
  DateMask = '^(������|�������|����|������|���|����|����|������|��������|�������|������|�������)\s\d{4}';
  DelimeterMask = '\s';
var
  NArr: TArray<String>;
  NMatch: TMatch;
begin
  Result := ADefault;
  try
    if TRegEx.IsMatch(AInput, DateMask, [roIgnoreCase]) then
    begin
      NMatch := TRegEx.Match(AInput, DateMask, [roIgnoreCase]);
      if NMatch.Success then
      begin
        NArr := TRegEx.Split(NMatch.Value, DelimeterMask);
        if Length(NArr) >= 2 then
          Result := EncodeDate(StrToInt(NArr[1]), GetMonthNum(NArr[0]), 1);
      end;
    end
  except
    Result := ADefault;
  end;
end;

function GetMonthNum(AMonthName: string): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 1 to Length(FormatSettings.LongMonthNames) do
  begin
    if String.Compare(AMonthName, FormatSettings.LongMonthNames[I], [coIgnoreCase]) = 0 then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function GetRusMonth(const ADate: TDate; const AShowYear: boolean = True): string;
var
  NYear, NMonth: Integer;
begin
  Result := EmptyStr;
  NMonth := MonthOf(ADate);
  Result := FormatSettings.LongMonthNames[NMonth];
  if AShowYear then
  begin
    NYear := YearOf(ADate);
    Result := Result + ' ' + IntToStr(NYear);
  end;
end;

function GetRusDate(const ADate: TDate): string;
const
  m: array [0 .. 11] of string = (
    '������', '�������', '�����', '������', '���', '����',
    '����', '�������', '��������', '�������', '������', '�������'
  );
var
  NDay, NMonth, NYear: Word;
begin
  Result := EmptyStr;
  if ADate > 0 then
  begin
    DecodeDate(ADate, NYear, NMonth, NDay);
    Result := IntToStr(NDay) + ' ' + m[NMonth-1] + ' ' + IntToStr(NYear);
  end;
end;

end.
