unit FILEHelper;

interface

uses
  System.SysUtils, System.Classes, System.RegularExpressions, Winapi.Windows, Winapi.ShellAPI;

function GetFileDateTime(const AFilePath: string): TDateTime;
function GetFileVersion(const AFilePath: string; const AMajorMinorOnly: boolean = false): string;
function GetFileList(ADir: string; const AFileMask: string = '*.*'): TStringList;
function CopyAllFiles(ASourceDir: string; ADesinationDir: string): Integer;
function MoveAllFiles(ASourceDir: string; ADesinationDir: string): Integer;
procedure OpenFolder(const APath: string; AHandle: HWND);
procedure OpenFileInFolder(const APath: string; AHandle: HWND);
/// <summary>��������� ������</summary>
/// <param name="AVers1">������ ���� "1.0"</param>
/// <param name="AVers2">������ ���� "1.0"</param>
/// <returns>
/// AVers1 < AVers2 => -1
/// AVers1 = AVers2 => 0
/// AVers1 > AVers2 => 1
/// </returns>
function CompareVersion(const AVers1, AVers2: string): Integer;

implementation

function GetFileDateTime(const AFilePath: String): TDateTime;
var
  NAge: Integer;
begin
  NAge := FileAge(AFilePath);
  if NAge = -1 then
    Result := 0
  else
    Result := FileDateToDateTime(NAge);
end;

function GetFileVersion(const AFilePath: String; const AMajorMinorOnly: boolean = false): String;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(AFilePath), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(AFilePath), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    if not AMajorMinorOnly then
    begin
      Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
      Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
    end;
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function GetFileList(ADir: String; const AFileMask: String = '*.*'): TStringList;
var
  NSearchRes: TSearchRec;
begin
  Result := TStringList.Create;
  ADir := IncludeTrailingPathDelimiter(ADir);
  if DirectoryExists(ADir) then
  begin
    if FindFirst(ADir + AFileMask, faAnyFile, NSearchRes) = 0 then
    begin
      repeat
        if not (NSearchRes.Attr in [faDirectory, faHidden, faSysFile]) then
          Result.Add(NSearchRes.Name);
      until
        FindNext(NSearchRes) <> 0;
      System.SysUtils.FindClose(NSearchRes);
    end;
  end;
end;

function CopyAllFiles(ASourceDir: string; ADesinationDir: string): Integer;
var
  NFile: string;
  NFileList: TStringList;
begin
  Result := 0;
  if DirectoryExists(ASourceDir) and DirectoryExists(ADesinationDir) then
  begin
    NFileList := GetFileList(ASourceDir);
    try
      for NFile in NFileList do
        if CopyFile(
          PChar(IncludeTrailingPathDelimiter(ASourceDir) + NFile),
          PChar(IncludeTrailingPathDelimiter(ADesinationDir) + NFile),
          False
        ) then Inc(Result);
    finally
      NFileList.Free;
    end;
  end;
end;

function MoveAllFiles(ASourceDir: string; ADesinationDir: string): Integer;
var
  NFile: string;
  NFileList: TStringList;
begin
  Result := 0;
  if DirectoryExists(ASourceDir) and DirectoryExists(ADesinationDir) then
  begin
    NFileList := GetFileList(ASourceDir);
    try
      for NFile in NFileList do
        if MoveFile(
          PChar(IncludeTrailingPathDelimiter(ASourceDir) + NFile),
          PChar(IncludeTrailingPathDelimiter(ADesinationDir) + NFile)
        ) then Inc(Result);
    finally
      NFileList.Free;
    end;
  end;
end;

procedure OpenFolder(const APath: string; AHandle: HWND);
begin
  if DirectoryExists(APath) then
    ShellExecute(AHandle, 'Open', PChar(APath), nil, nil, SW_SHOWNORMAL)
  else
    MessageBox(AHandle, PChar(APath + #13 + '���������� �� ����������'), '������', MB_OK + MB_ICONERROR)
end;

procedure OpenFileInFolder(const APath: string; AHandle: HWND);
begin
  if FileExists(APath) then
    ShellExecute(AHandle, 'Open', 'explorer.exe', PChar('/select, ' + APath), PChar(APath), SW_SHOWNORMAL)
  else
    MessageBox(AHandle, PChar(APath + #13 + '���� �� ������'), '������', MB_OK + MB_ICONERROR);
end;

function CompareVersion(const AVers1, AVers2: string): Integer;
const
  VersionMask = '^\d+\.\d+$';
  DelimeterMask = '\.';
var
  NArr: TArray<String>;
  NVMaj1, NVMin1: Integer;
  NVMaj2, NVMin2: Integer;
begin
  Result := -100500;
  if TRegEx.IsMatch(AVers1, VersionMask) and TRegEx.IsMatch(AVers2, VersionMask) then
  begin
    NArr := TRegEx.Split(AVers1, DelimeterMask);
    if Length(NArr) = 2 then
    begin
      NVMaj1 := StrToInt(NArr[0]);
      NVMin1 := StrToInt(NArr[1]);
    end;
    NArr := TRegEx.Split(AVers2, DelimeterMask);
    if Length(NArr) = 2 then
    begin
      NVMaj2 := StrToInt(NArr[0]);
      NVMin2 := StrToInt(NArr[1]);
    end;
    if NVMaj1 < NVMaj2 then
      Result := -1
    else if NVMaj1 > NVMaj2 then
      Result := 1
    else
    begin
      if NVMin1 < NVMin2 then
        Result := -1
      else
      if NVMin1 > NVMin2 then
        Result := 1
      else
        Result := 0
    end;

  end
end;

end.
