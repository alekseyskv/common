﻿unit MSWord;

// http://www.webdelphi.ru/2010/02/microsoft-word-v-delphi/

interface

uses
  System.Variants, System.Classes, System.SysUtils, System.Win.ComObj, Winapi.ActiveX, WordXP;

type
  TWord = class(TObject)
    FWord: OleVariant;     // Application Object (Word)
    private
      function FAndR(const AFindText, AReplaceText: string): Boolean;
    public
      constructor Create(AVisible: Boolean = False);
      destructor Destroy; override;

      procedure FindAndReplace(const AFindText, AReplaceText: string);
      function AddTable(ARowCount, AColCount: Integer): Variant;
      function SaveAsAndClose(AFilePath: string): Boolean;
      function GetTableCount: Integer;
      procedure InsertTable(AFindText: string; ARowCount, AColCount: Integer; const ATableID: string = '');
      procedure SetTableCelValue(ATableIndex, ARow, ACol: Integer; AValue: string); overload;
      procedure SetTableCelValue(ATableID: string; ARow, ACol: Integer; AValue: string); overload;
      procedure MergeCells(ATableTitle: string; ARowBeg, AColBeg, ARowEnd, AColEnd: Integer);
      procedure MergeRow(ATableTitle: string; ARow: Integer; AAlign: Integer = 1); overload;
      procedure MergeRow(ATableIndex: Integer; ARow: Integer; AAlign: Integer = 1); overload;
      procedure SplitRow(ATableIndex: Integer; ARow: Integer);
      function CloneTable(ATableIndex: Integer; ANewTableTitle: string): Integer;
      function DeleteTable(ATableIndex: Integer): Boolean;
      function AddTextBeforeTable(ATableIndex: Integer; AText: string): Boolean;
      function SetTableTitle(ATableIndex: Integer; ATableTitle: string): Boolean;
      function GetTableIndex(ATableTitle: string): Integer;
      function ExistTable(ATableTitle: string): Boolean;
      function AddRowToTable(ATableIndex: Integer; AStrings: TStringList): Integer; overload;
      function AddRowToTable(ATableTitle: string; AStrings: TStringList): Integer; overload;
      function AddEmptyRowToTable(ATableTitle: string): Integer;
      procedure AddParagraph(AString: string);
      procedure Print;

      function Quit: Boolean;
      procedure NewDoc; overload;
      procedure NewDoc(ATemplatePath: string); overload;
      procedure OpenDoc(AFilePath: string; AReadOnly: Boolean = True);
      procedure Show;
      procedure Hide;
      procedure CloseAllDocs;
      class function CheckInstall: Boolean; static;
  end;

const
  WordApp = 'Word.Application';
  wdLineStyleSingle = 1;

  wdFormatDocument=0;
  wdFormatTemplate=1;
  wdFormatText=2;
  wdFormatTextLineBreaks=3;
  wdFormatDOSText=4;
  wdFormatDOSTextLineBreaks=5;
  wdFormatRTF=6;
  wdFormatUnicodeText=7;
  // https://msdn.microsoft.com/ru-ru/library/office/microsoft.office.interop.word.aspx
  wdAlignParagraphLeft=0;
  wdAlignParagraphCenter=1;
  wdAlignParagraphRight=2;
  wdAlignParagraphJustify=3;

  wdAlignRowLeft = 0;
  wdAlignRowCenter = 1;
  wdAlignRowRight = 2;

  // https://msdn.microsoft.com/en-us/VBA/Word-VBA/articles/wdrecoverytype-enumeration-word
  wdSingleCellTable=6;
  wdFormatOriginalFormatting=16;

  NP = '^p'; // новый параграф
  NL = '^l'; // новая строка
  CLRF = #$D#$A; //#13#10

function ReplaceCLRF(AString: string): string;

implementation

function ReplaceCLRF(AString: string): string;
begin
  Result := StringReplace(AString, CLRF, NL, [rfReplaceAll]);
end;

{ TWord }

constructor TWord.Create(AVisible: Boolean);
begin
  inherited Create;
  FWord := CreateOleObject(WordApp);
  FWord.Visible := AVisible;
  // WdAlertLevel Enumeration:
  // wdAlertsAll = -1
  // wdAlertsMessageBox = -2
  // wdAlertsNone = 0
  FWord.DisplayAlerts := 0;
end;

destructor TWord.Destroy;
begin
  FWord := Unassigned;
  inherited;
end;

function TWord.GetTableIndex(ATableTitle: string): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 1 to FWord.ActiveDocument.Tables.Count do
  begin
    if FWord.ActiveDocument.Tables.Item(I).Title = ATableTitle then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TWord.ExistTable(ATableTitle: string): Boolean;
begin
  Result := GetTableIndex(ATableTitle) > 0;
end;

function TWord.CloneTable(ATableIndex: Integer; ANewTableTitle: string): Integer;
var
  tbl: Variant;
begin
  Result := 0;
  if ATableIndex > 0 then
  begin
    // исходная таблица
    tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
    if VarIsNull(tbl) then
      Exit;

    tbl.Select; // выделяем таблицу
    FWord.Selection.Copy; // копируем в буфер
    FWord.Selection.Collapse(wdCollapseEnd); // снимаем выделение, курсор в конец
    FWord.Selection.TypeParagraph; // перенос строки
    FWord.Selection.Paste; // вставляем из буфера

    // новая таблица
    tbl := FWord.ActiveDocument.Tables.Item(ATableIndex + 1);
    if VarIsNull(tbl) then
      Exit;

    tbl.Title := ANewTableTitle;

    Result := ATableIndex + 1;

    // посмотреть:
    // MSWord.Selection.MoveDown( Unit:=wdLine, Count:=1);
    // MSWord.Selection.GoTo(What:= wdGoToTable, Which:=wdGoToFirst, Count:=3);
  end;
end;

function TWord.AddTextBeforeTable(ATableIndex: Integer; AText: string): Boolean;
var
  tbl: Variant;
begin
  Result := False;
  if ATableIndex > 0 then
  begin
    try
      tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
      if VarIsNull(tbl) then
        Exit;

      tbl.Select; // выделяем таблицу
      FWord.Selection.MoveUp(Unit:=wdLine, Count:=1);
      FWord.Selection.TypeParagraph;
      FWord.Selection.TypeText(AText);
      Result := True;
    finally
      Result := False;
    end;
  end;
end;

function TWord.DeleteTable(ATableIndex: Integer): Boolean;
var
  tbl: Variant;
begin
  Result := False;
  if ATableIndex > 0 then
  begin
    try
      tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
      if VarIsNull(tbl) then
        Exit;
      tbl.Delete;
      Result := True;
    except
      Result := False;
    end;
  end;
end;

procedure TWord.CloseAllDocs;
begin
  if FWord.Documents.Count > 0 then
    FWord.Documents.Close;
//  for I:=1 to FWord.Documents.Count do
//    FWord.Documents.Item(I).Close;
end;

procedure TWord.NewDoc;
begin
  FWord.Documents.Add.Activate;
end;

procedure TWord.NewDoc(ATemplatePath: string);
begin
  FWord.Documents.Add(ATemplatePath).Activate;
end;

procedure TWord.OpenDoc(AFilePath: string; AReadOnly: Boolean = True);
begin
  FWord.Documents.Open(FileName := AFilePath, ReadOnly := AReadOnly).Activate;
end;

procedure TWord.Print;
begin
  FWord.ActiveDocument.PrintOut;
end;

function TWord.Quit: Boolean;
begin
  try
    if FWord.Visible then
      FWord.Visible := False;
    FWord.Quit;
    Result:=True;
  except
    Result:=False;
  end;
end;

function TWord.SaveAsAndClose(AFilePath: string): Boolean;
begin
  FWord.ActiveDocument.SaveAs(FileName := AFilePath, FileFormat := WdFormatDocument);
  Result := FWord.ActiveDocument.Saved;
  FWord.ActiveDocument.Close;
end;

procedure TWord.Show;
begin
  FWord.Visible := True;
end;

procedure TWord.Hide;
begin
  FWord.Visible := False;
end;

function TWord.AddTable(ARowCount, AColCount: Integer): Variant;
var
  endDoc: Integer;
begin
  endDoc := FWord.ActiveDocument.Range.End - 1;
  Result := FWord.ActiveDocument.Tables.Add(
    Range:=FWord.ActiveDocument.Range(endDoc, endDoc)
    , NumRows := ARowCount
    , NumColumns := AColCount
  );
  //Параметры линий таблицы.
  Result.Borders.InsideLineStyle := wdLineStyleSingle;
  Result.Borders.OutsideLineStyle := wdLineStyleSingle;
end;

function TWord.AddRowToTable(ATableIndex: Integer; AStrings: TStringList): Integer;
var
  I: Integer;
  tbl, row: Variant;
begin
  Result := 0;

  if ATableIndex > FWord.ActiveDocument.Tables.Count then
    Exit;

  tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
  if VarIsNull(tbl) then
    Exit;

  row := tbl.Rows.Add;
  row.AllowBreakAcrossPages := False;
//  row.Alignment := wdAlignRowRight;
  row.Range.ParagraphFormat.Alignment := wdAlignParagraphLeft;

  Result := row.Index;

  for I := 1 to row.Cells.Count do
  begin
    if AStrings.Count > (I-1) then
      row.Cells.Item(I).Range.Text := AStrings.Strings[I-1];
  end;
  row.Height := 0;
  row.HeightRule := wdRowHeightAtLeast;
end;

function TWord.AddRowToTable(ATableTitle: string; AStrings: TStringList): Integer;
begin
  Result := AddRowToTable(GetTableIndex(ATableTitle), AStrings);
end;

function TWord.AddEmptyRowToTable(ATableTitle: string): Integer;
var
  TblIndex: Integer;
  tbl, row: Variant;
begin
  Result := 0;
  TblIndex := GetTableIndex(ATableTitle);
  tbl := FWord.ActiveDocument.Tables.Item(TblIndex);
  if VarIsNull(tbl) then
    Exit;
  row := tbl.Rows.Add;
  row.AllowBreakAcrossPages := False;
  Result := row.Index;
end;

procedure TWord.AddParagraph(AString: string);
var
  endDoc: integer;
begin
  endDoc := FWord.ActiveDocument.Range.End - 1;
  FWord.Selection.Start := endDoc;
  FWord.Selection.End := endDoc;
  FWord.Selection.TypeText(AString);
  FWord.Selection.TypeParagraph;
  //MyWord.Selection.TypeText('привет медвед');
  //MyWord.Selection.TypeText(#13#10'new');
end;

procedure TWord.InsertTable(AFindText: string; ARowCount, AColCount: Integer; const ATableID: string = '');
var
  tbl: Variant;
begin
  FWord.Selection.Find.Forward := true;
  FWord.Selection.Find.Text := AFindText;

  while FWord.Selection.Find.Execute do
  begin
    tbl := FWord.ActiveDocument.Tables.Add(
      Range:=FWord.Selection.Range
      , NumRows := ARowCount
      , NumColumns := AColCount
    );
    // штоб ячейка таблицы не разрывалась по строкам между страницами
    FWord.Selection.ParagraphFormat.KeepWithNext := True;
    //Параметры линий таблицы.
    tbl.Borders.InsideLineStyle := wdLineStyleSingle;
    tbl.Borders.OutsideLineStyle := wdLineStyleSingle;
    //ID
    if ATableID <> '' then
      tbl.ID := ATableID;
  end;
end;

function TWord.GetTableCount: Integer;
begin
  Result := FWord.ActiveDocument.Tables.Count;
end;

procedure TWord.SetTableCelValue(ATableIndex, ARow, ACol: Integer; AValue: string);
var
  tbl: Variant;
begin
  if ATableIndex > FWord.ActiveDocument.Tables.Count then
    Exit;

  tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);

  if (ARow > tbl.Rows.Count) or (ACol > tbl.Columns.Count) then
    Exit;

  tbl.Cell(ARow, ACol).Range.Text := AValue;

  //tbl.Cell(Row,Col).VerticalAlignment := integer;
  //tbl.Cell(Row,Col).Range.ParagraphFormat.Alignment := wdAlignParagraphLeft;
end;

procedure TWord.SetTableCelValue(ATableID: string; ARow, ACol: Integer; AValue: string);
var
  I: Integer;
  tbl: Variant;
begin
  for I := 1 to FWord.ActiveDocument.Tables.Count do
  begin
    if FWord.ActiveDocument.Tables.Item(I).ID = ATableID then
    begin
      tbl := FWord.ActiveDocument.Tables.Item(I);

      if VarIsNull(tbl) then
        exit;

      if (ARow > tbl.Rows.Count) or (ACol > tbl.Columns.Count) then
        exit;

      tbl.Cell(ARow, ACol).Range.Text := AValue;
      tbl.Cell(ARow, ACol).Range.ParagraphFormat.Alignment := wdAlignParagraphLeft;
    end;
  end;
  //tbl.Cell(ARow, ACol).VerticalAlignment := integer;
end;

function TWord.SetTableTitle(ATableIndex: Integer; ATableTitle: string): Boolean;
var
  tbl: Variant;
begin
  Result := False;
  if ATableIndex > 0 then
  begin
    tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
    if VarIsNull(tbl) then
      Exit;

    tbl.Title := ATableTitle;
    Result := True;
  end;
end;

procedure TWord.MergeCells(ATableTitle: string; ARowBeg, AColBeg, ARowEnd, AColEnd: Integer);
var
  TblIndex: Integer;
  tbl: Variant;
begin
  TblIndex := GetTableIndex(ATableTitle);
  if TblIndex > 0 then
  begin
    tbl := FWord.ActiveDocument.Tables.Item(TblIndex);
    if VarIsNull(tbl) then
      Exit;

    tbl.Cell(ARowBeg,AColBeg).Merge(tbl.Cell(ARowEnd,AColEnd));
  end;
end;

procedure TWord.MergeRow(ATableTitle: string; ARow: Integer; AAlign: Integer = 1);
var
  TblIndex: Integer;
begin
  TblIndex := GetTableIndex(ATableTitle);
  MergeRow(TblIndex, ARow, AAlign);
end;

procedure TWord.MergeRow(ATableIndex: integer; ARow: Integer; AAlign: Integer = 1);
var
  tbl: Variant;
begin
  if ATableIndex > FWord.ActiveDocument.Tables.Count then
    Exit;

  tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
  if VarIsNull(tbl) then
    Exit;

  if tbl.Rows.Count < ARow then
    Exit;

  tbl.Cell(ARow,1).Merge(tbl.Cell(ARow,tbl.Columns.Count));
  tbl.Rows[ARow].Range.ParagraphFormat.Alignment := AAlign;
//  или так
//  MyWord.ActiveDocument.Range(
//    tbl.Cell(ARow, 1).Range.Start,
//    tbl.Cell(ARow, tbl.Columns.Count).Range.End
//  ).Cells.Merge;
end;

procedure TWord.SplitRow(ATableIndex, ARow: Integer);
var
  tbl: Variant;
begin
  if ATableIndex > FWord.ActiveDocument.Tables.Count then
    Exit;

  tbl := FWord.ActiveDocument.Tables.Item(ATableIndex);
  if VarIsNull(tbl) then
    Exit;

  if tbl.Rows.Count < ARow then
    Exit;

//  tbl.Rows[ARow].Cells.Split;
//  (
//    NumRows := 1,
//    NumColumns := tbl.Columns.Count,
//    MergeBeforeSplit := False
//  );

  if tbl.Columns.Count > tbl.Rows[ARow].Cells.Count then
  begin
    FWord.ActiveDocument.Range(
      tbl.Cell(ARow, 1).Range.Start,
      tbl.Cell(ARow, tbl.Rows[ARow].Cells.Count).Range.End
    ).Select;
    FWord.Selection.Cells.Split(
      NumRows := 1,
      NumColumns := tbl.Columns.Count,
      MergeBeforeSplit := False
    );
  end;

  //(NumRows := 1ARow,tbl.Columns.Count);
  //Selection.Cells.Split NumRows:=1, NumColumns:=2, MergeBeforeSplit:=False
end;

// Если Length(ReplaceText) > 255 -> Error: Слишком длинный сроковый параметр
//
//function TWord.FindAndReplace(const FindText, ReplaceText: string):boolean;
//const
//  wdReplaceAll = 2;
//begin

//end;

function TWord.FAndR(const AFindText, AReplaceText: string): Boolean;
const
  wdReplaceAll = 2;
begin
  FWord.Selection.Find.MatchSoundsLike := False;
  FWord.Selection.Find.MatchAllWordForms := False;
  FWord.Selection.Find.MatchWholeWord := False;
  FWord.Selection.Find.Format := False;
  FWord.Selection.Find.Forward := True;
  FWord.Selection.Find.ClearFormatting;
  FWord.Selection.Find.Text := AFindText;
  FWord.Selection.Find.Replacement.Text := AReplaceText;
  Result := FWord.Selection.Find.Execute(Replace := wdReplaceAll);
end;

procedure TWord.FindAndReplace(const AFindText, AReplaceText: string);
var
  itemText: string;
  itemLenght: integer;
  find, replace: string;
begin
  find := AFindText;
  replace := AReplaceText;
  if Length(replace) > 255 then
  begin
    itemLenght := 255 - Length(find);
    while Length(replace) > 0 do
    begin
      if Length(replace) <= itemLenght then
      begin
        itemLenght := Length(replace);
        find := '';
      end;
	  itemText := Copy(replace, 1, itemLenght);
      if itemText[Length(itemText)] = '^' then
      begin
        itemLenght := itemLenght - 1;
        itemText := Copy(replace, 1, itemLenght);
      end;
      itemText := itemText + find;
      Delete(replace, 1, itemLenght);
      FAndR(AFindText, itemText);
    end;
  end
  else FAndR(AFindText, AReplaceText);
end;

class function TWord.CheckInstall:boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin
  Rez := CLSIDFromProgID(PWideChar(WideString(WordApp)), ClassID);
  if Rez = S_OK then
    Result := true
  else
    Result := false;
end;

end.
