unit GSPCalc;

interface

uses
  System.SysUtils, System.Math;

type
  TGSPInputData = record
    // ��� ����
    // 0 - ��� ����� ���������� ��� ������� �����
    // 1 - ����������� ���
    CourtType: Integer;
    // ���������
    // 0 - ������ ���������
    // 1 - ���������/�������
    // 2 - ��������� ���������
    Instance: Integer;
    // ������ �����������
    // 0 - ��
    // 1 - ��
    PayerType: Integer;
    // �������� ������
    IsCourtCommand: Boolean;
    // ����� �����
    DebtSum: Currency;
    function Dump: string;
    function GetIsCourtCommand: Boolean;
  end;

  TGSPResultData = record
    Value: Currency;
    CalcWay: string;
  end;

const
  SOU_LIMIT_1 = 20000;
  SOU_LIMIT_2 = 100000;
  SOU_LIMIT_3 = 200000;
  SOU_LIMIT_4 = 1000000;

  AS_LIMIT_1 = 100000;
  AS_LIMIT_2 = 200000;
  AS_LIMIT_3 = 1000000;
  AS_LIMIT_4 = 2000000;

function GSPCalcDigit(AOptions: TGSPInputData): Currency;
function GSPCalculate(AOptions: TGSPInputData; ACalcWay: Boolean = False): TGSPResultData;

implementation

function GSPCalcDigit(AOptions: TGSPInputData): Currency;
begin
  Result := 0;
  if AOptions.Instance = 0 then // ������ ���������
  begin
    if AOptions.CourtType = 0 then
    begin
      // ��� ����� ����������
      if AOptions.DebtSum < SOU_LIMIT_1 + 1 then
      begin
        Result := AOptions.DebtSum * 0.04;
        if Result < 400 then Result := 400;
      end
      else if AOptions.DebtSum < SOU_LIMIT_2 + 1  then
      begin
        Result := (AOptions.DebtSum - SOU_LIMIT_1) * 0.03 + 800;
      end
      else if AOptions.DebtSum < SOU_LIMIT_3 + 1 then
      begin
        Result := (AOptions.DebtSum - SOU_LIMIT_2) * 0.02 + 3200;
      end
      else if AOptions.DebtSum < SOU_LIMIT_4 + 1 then
      begin
        Result := (AOptions.DebtSum - SOU_LIMIT_3) * 0.01 + 5200;
      end
      else
      begin
        Result := (AOptions.DebtSum - SOU_LIMIT_4) * 0.005 + 13200;
        if Result > 60000 then Result := 60000;
      end;
    end
    else
    begin
      // ����������� ���
      if AOptions.DebtSum < AS_LIMIT_1 + 1 then
      begin
        Result := AOptions.DebtSum * 0.04;
        if Result < 2000 then Result := 2000;
      end
      else if AOptions.DebtSum < AS_LIMIT_2 + 1 then
      begin
        Result := (AOptions.DebtSum - AS_LIMIT_1) * 0.03 + 4000;
      end
      else if AOptions.DebtSum < AS_LIMIT_3 + 1 then
      begin
        Result := (AOptions.DebtSum - AS_LIMIT_2) * 0.02 + 7000;
      end
      else if AOptions.DebtSum < AS_LIMIT_4 + 1 then
      begin
        Result := (AOptions.DebtSum - AS_LIMIT_3) * 0.01 + 23000;
      end
      else
      begin
        Result := (AOptions.DebtSum - AS_LIMIT_4) * 0.005 + 33000;
        if Result > 200000 then Result := 200000;
      end;
    end;
    // � 01.06.2016 �������� ������ ����������� ��� ����� ����� ������ ���������
    if AOptions.IsCourtCommand
    then Result := Result/2;
  end;

  if AOptions.Instance = 1 then // ������ ���������
  begin
    if AOptions.CourtType = 0 then
      if AOptions.PayerType = 0
      then Result := 3000
      else Result := 150
    else Result := 3000;
  end;

  if AOptions.Instance = 2 then // ��������� ���������
  begin
    if AOptions.CourtType = 0 then
      if AOptions.PayerType = 0
      then Result := 6000
      else Result := 300
    else Result := 3000;
  end;
  Result := RoundTo(Result, -2);
end;

function GSPCalculate(AOptions: TGSPInputData; ACalcWay: Boolean = False): TGSPResultData;
  function TrueCurrToStr(AValue: Currency): string;
  begin
    Result := Format('%15.2f',[RoundTo(AValue, -2)]);
  end;
var
  temp: Currency;
begin
  Result.Value := 0;
  Result.CalcWay := EmptyStr;

  with Result do
  begin
    if AOptions.Instance = 0 then // ������ ���������
    begin
      if AOptions.CourtType = 0 then
      begin
        // ��� ����� ����������
        if AOptions.DebtSum < SOU_LIMIT_1 + 1 then
        begin
          Value := AOptions.DebtSum * 0.04;
          if ACalcWay then
            CalcWay := Format('4 ����. �� %s ���. = %s ���.',[TrueCurrToStr(AOptions.DebtSum), TrueCurrToStr(Value)]);

          if Value < 400 then Value := 400;
          if ACalcWay then
            CalcWay := CalcWay + Format(' ������� %s ���.',[TrueCurrToStr(Value)]);
        end
        else if AOptions.DebtSum < SOU_LIMIT_2 + 1 then
        begin
          Value := (AOptions.DebtSum - SOU_LIMIT_1) * 0.03 + 800;

          if ACalcWay then
            CalcWay := Format('800 ���. + 3 ����. �� (%s ���.-%s ���.) = %s ���. ������� %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(SOU_LIMIT_1),TrueCurrToStr(Value),TrueCurrToStr(Value)]);
        end
        else if AOptions.DebtSum < SOU_LIMIT_3 + 1 then
        begin
          Value := (AOptions.DebtSum - SOU_LIMIT_2) * 0.02 + 3200;

          if ACalcWay then
            CalcWay := Format('3200 ���. + 2 ����. �� (%s ���.-%s ���.) = %s ���. ������� %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(SOU_LIMIT_2),TrueCurrToStr(Value),TrueCurrToStr(Value)]);
        end
        else if AOptions.DebtSum < SOU_LIMIT_4 + 1 then
        begin
          Value := (AOptions.DebtSum - SOU_LIMIT_3) * 0.01 + 5200;

          if ACalcWay then
            CalcWay := Format('5200 ���. + 1 ����. �� (%s ���.-%s ���.) = %s ���. ������� %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(SOU_LIMIT_3),TrueCurrToStr(Value),TrueCurrToStr(Value)]);
        end
        else
        begin
          Value := (AOptions.DebtSum - SOU_LIMIT_4) * 0.005 + 13200;
          if ACalcWay then
            CalcWay := Format('13200 ���. + 0.5 ����. �� (%s ���.-%s ���.) = %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(SOU_LIMIT_4),TrueCurrToStr(Value)]);

          if Value > 60000 then Value := 60000;
          if ACalcWay then
            CalcWay := CalcWay + Format(' ������� %s ���.',[TrueCurrToStr(Value)])
        end;
      end
      else
      begin
        // ����������� ���
        if AOptions.DebtSum < AS_LIMIT_1 + 1 then
        begin
          Value := AOptions.DebtSum * 0.04;
          if ACalcWay then
            CalcWay := Format('4 ����. �� %s ���. = %s ���.',[TrueCurrToStr(AOptions.DebtSum),TrueCurrToStr(Value)]);

          if Value < 2000 then Value := 2000;
          if ACalcWay then
            CalcWay := CalcWay + Format(' ������� %s ���.',[TrueCurrToStr(Value)]);
        end
        else if AOptions.DebtSum < AS_LIMIT_2 + 1 then
        begin
          Value := (AOptions.DebtSum - AS_LIMIT_1) * 0.03 + 4000;

          if ACalcWay then
            CalcWay := Format('4000 ���. + 3 ����. �� (%s ���.-%s ���.) = %s ���. ������� %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(AS_LIMIT_1),TrueCurrToStr(Value),TrueCurrToStr(Value)]);
        end
        else if AOptions.DebtSum < AS_LIMIT_3 + 1 then
        begin
          Value := (AOptions.DebtSum - AS_LIMIT_2) * 0.02 + 7000;

          if ACalcWay then
            CalcWay := Format('7000 ���. + 2 ����. �� (%s ���.-%s ���.) = %s ���. ������� %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(AS_LIMIT_2),TrueCurrToStr(Value),TrueCurrToStr(Value)]);
        end
        else if AOptions.DebtSum < AS_LIMIT_4 + 1 then
        begin
          Value := (AOptions.DebtSum - AS_LIMIT_3) * 0.01 + 23000;

          if ACalcWay then
            CalcWay := Format('23000 ���. + 1 ����. �� (%s ���.-%s ���.) = %s ���. ������� %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(AS_LIMIT_3),TrueCurrToStr(Value),TrueCurrToStr(Value)]);
        end
        else
        begin
          Value := (AOptions.DebtSum - AS_LIMIT_4) * 0.005 + 33000;

          if ACalcWay then
            CalcWay := Format('33000 ���. + 0.5 ����. �� (%s ���.-%s ���.) = %s ���.',
              [TrueCurrToStr(AOptions.DebtSum),IntToStr(AS_LIMIT_4),TrueCurrToStr(Value)]);

          if Value > 200000 then Value := 200000;
          if ACalcWay then
            CalcWay := CalcWay +Format(' ������� %s ���.',[TrueCurrToStr(Value)]);
        end;
      end;
      // � 01.06.2016 �������� ������ ����������� ��� ����� ����� ������ ���������
      if AOptions.IsCourtCommand then
      begin
        temp := Value;
        Value := Value / 2;
        if ACalcWay then
          CalcWay := CalcWay + Format(' ������� 50 ����. �� %s ���. = %s ���.',
            [TrueCurrToStr(temp), TrueCurrToStr(Value)]);
      end;
    end;

    if AOptions.Instance = 1 then // ������ ���������
    begin
      if AOptions.CourtType = 0 then
        if AOptions.PayerType = 0 then
        begin
          Value := 3000;
          if ACalcWay then
            CalcWay := '������� ��� ��.���� 3000 ���.' // 50% �� 6000 ���
        end
        else
        begin
          Value := 150;
          if ACalcWay then
            CalcWay := '������� ��� ���.���� 150 ���.' // 50% �� 300 ���
        end
      else
      begin
        Value := 3000;
        if ACalcWay then
          CalcWay := '������� 3000 ���.'               // 50% �� 6000 ���
      end;
    end;

    if AOptions.Instance = 2 then // ��������� ���������
    begin
      if AOptions.CourtType = 0 then
        if AOptions.PayerType = 0 then
        begin
          Value := 6000;
          if ACalcWay then
            CalcWay := '������� ��� ��.���� 6000 ���.'
        end
        else
        begin
          Value := 300;
          if ACalcWay then
            CalcWay := '������� ��� ���.���� 300 ���.'
        end
      else
      begin
        Value := 3000;
        if ACalcWay then
          CalcWay := '������� 3000 ���.'               // 50% �� 6000 ���
      end;
    end;

    Value := RoundTo(Value, -2);
  end;
end;

{ TGSPInputData }

function TGSPInputData.Dump: string;
begin
  Result := '���:' + IntToStr(CourtType) + ';';
  Result := Result + '���������:' + IntToStr(Instance) + ';';
  Result := Result + '����������:' + IntToStr(PayerType) + ';';
  Result := Result + '������:' + IntToStr(Ord(IsCourtCommand)) + ';';
  Result := Result + '�����:' + FloatToStr(DebtSum);
end;

function TGSPInputData.GetIsCourtCommand: Boolean;
begin
  Self.IsCourtCommand := True;
  // ( ����������� ��� ) ��� ( ����� > 500 ���.������ )
  if (Self.CourtType = 1) or (Self.DebtSum > 500000) then
    Self.IsCourtCommand := False;
  Result := Self.IsCourtCommand;
end;

end.
