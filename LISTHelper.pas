unit LISTHelper;

interface

uses
  System.SysUtils, System.Generics.Collections;

type
  TGroupItem<T: class> = class(TObjectList<T>)
  protected
    FGroupField: string;
  public
    property GroupField: string read FGroupField write FGroupField;
  end;

  TGroupList<T: class> = class(TObjectList<TGroupItem<T>>)
  protected
    function GetMaxItemCount: Integer;
  public
    property MaxItemCount: Integer read GetMaxItemCount;
    function Dump: string;
    function ExistList(AGroupValue: string): Boolean;
    function GetList(AGroupValue: string): TGroupItem<T>;
    function CreateList(AGroupValue: string): TGroupItem<T>;
  end;

implementation

{ TGroupList<T> }

function TGroupList<T>.CreateList(AGroupValue: string): TGroupItem<T>;
begin
  Result := TGroupItem<T>.Create;
  Self.Add(Result);
  Result.GroupField := AGroupValue;
end;

function TGroupList<T>.Dump: string;
var
  I: Integer;
begin
  Result := EmptyStr;
  for I := 0 to Self.Count - 1 do
    Result := Result + Self.Items[I].GroupField + ':' + IntToStr(Self.Items[I].Count) + #$D#$A;
end;

function TGroupList<T>.ExistList(AGroupValue: string): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Self.Count - 1 do
  begin
    if Self.Items[I].GroupField = AGroupValue then
    begin
      Result := True;
      Break;
    end;
  end;
end;


function TGroupList<T>.GetList(AGroupValue: string): TGroupItem<T>;
var
  I: Integer;
begin
  for I := 0 to Self.Count - 1 do
  begin
    if Self.Items[I].GroupField = AGroupValue then
    begin
      Result := Self.Items[I];
      Break;
    end;
  end;
end;

function TGroupList<T>.GetMaxItemCount: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to Self.Count - 1 do
    if Self.Items[I].Count > Result then
      Result := Self.Items[I].Count;
end;

end.
